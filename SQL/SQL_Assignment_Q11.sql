﻿DELIMITER $$

DROP PROCEDURE IF EXISTS `drs`.`EMIq11` $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `EMIq11`(in p_amt varchar(10),in interest varchar(10),in years varchar(10))
begin
declare emi_amount float(8,2);
declare months int default 0;
declare intItr int default 1;
declare varX float;
declare previous_balance float(10,2);
declare new_balance float(10,2);
declare to_p_amt float(7,2);
declare to_interest_amt float(7,2);
truncate table EmiCalculator;
if p_amt ='' || p_amt regexp '^[0]' || p_amt regexp '[a-zA-Z]'



then



	select 'Please enter valid principle amount' as error;



else if interest ='' || interest regexp '^[0]' || interest regexp '[a-zA-Z]'  || length(interest)>4 || length(substring_index(interest,'.',1))>2 && length(substring_index(interest,'.',-1))>2



then



	select 'Please enter valid rate of interest' as error;



else if years ='' || years regexp '^[0]' || years regexp '[a-zA-Z]' || length(substring_index(years,'.',1))>2





then



select 'Please enter valid number of years' as error;



else



begin



set months=years*12;

set interest=(interest/1200);

set varx=pow((1+interest),months);



set emi_amount=round((p_amt * interest * varX)/( varX - 1 ),2);

set previous_balance=p_amt;



while intItr <= months



	do

		set to_interest_amt=previous_balance*interest;



		set to_p_amt=emi_amount-to_interest_amt;



		set new_balance=previous_balance-to_p_amt;



		set previous_balance=new_balance;



		if new_balance < 0



			then set new_balance=0.00;



	end if;
	insert into EmiCalculator values(intItr,emi_amount,to_p_amt,to_interest_amt,new_balance);
	set intItr=intItr+1;
	end while;
end;
end if;
end if;
end if;
select * from EmiCalculator;
end $$

DELIMITER ;