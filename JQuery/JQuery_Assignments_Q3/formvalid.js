function validate(){
		var fname=document.getElementById("firstname").value;
		var lname=document.getElementById("lastname").value;
		var uname=document.getElementById("username").value
		var dob=document.getElementById("DOB").value;
		var email=document.getElementById("email").value;
		var phone=document.getElementById("pnumber").value;
		var state=document.getElementById("state").value;			
		var name_regex = /^[a-zA-Z]+$/;
		var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		var hobbie=document.getElementById("hobbie").value;
		//fname validation
		if(fname==""){
			alert("Please fill the first name");
			return false;
		}
		else if(!fname.match(name_regex)){
			alert("Please firstname in alphabetical format");
			return false;
		}
		//lname validation
		else if(lname=="")
		{
			alert("Please fill the last name");
			return false;
		}
		
		else if(!lname.match(name_regex)){
			alert("Please last name in alphabetical format");
			return false;
		}
		
		//uname validation
		else if(uname=="")
		{
			alert("Please fill the username");
			return false;
		}
		else if(!uname.match(name_regex)){
			alert("Please user name in alphabetical format");
			return false;
		}
		//
		else if(dob=="")
		{
			alert("Please fill the date of birth in(MM/DD/YYYY) Format");
			return false;
		}
		
		//email validation
		else if(email=="")
		{
			alert("enter the email_id ");
			return false;
		}
		else if(!email.match(email_regex))
		{
			alert("enter email id in proper format");
			return false;
			
		}
		
		else if(phone==""){
			alert("Please enter phone");
			return false;
		}
		else if(phone.length!=10||phone.match(name_regex)){
			alert("Enter phone number in 222-222-2222 format");
			return false;
		}
		//validation of select field.
		else if (state =="Select"){
			alert("Please choose country");
			return false;
		}
		
		else{
			alert("Form submitted successfully");
			return true;
		}
	$(document).ready(function(){
	$("#disable").parent().on("click","#disable",function () {
	$("#target input").prop("disabled",true);
		
	$('#disable').prop("disabled",false);
    $("#disable").val("ENABLE");
    $("#disable").prop("id", "enable");
	});

	$("#disable").parent().on("click","#enable",function () {
	$("#target input").prop("disabled",false);
    $("#enable").val("DISABLE");
    $("#enable").prop("id", "disable");    
	});
	});
		
	}
	
